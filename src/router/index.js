import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("../views/Home.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/user/Login.vue"),
  },
  {
    path: "/register",
    name: "Register",
    component: () => import("../views/user/Register.vue"),
  },
  {
    path: "/logout",
    name: "Logout",
    component: () => import("../views/user/Logout.vue"),
  },
  {
    path: "/me",
    name: "Profile",
    component: () => import("../views/user/Profile.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/my_products",
    name: "MyProducts",
    component: () => import("../views/product/MyProducts.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/products/:id",
    name: "ProductDetails",
    component: () => import("../views/product/ProductDetails.vue"),
  },
  {
    path: "/sell",
    name: "Sell",
    component: () => import("../views/product/Sell.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/auction",
    name: "Auction",
    component: () => import("../views/product/Auction.vue"),
    meta: {
      requiresAuth: true,
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    const loggedIn = JSON.parse(localStorage.getItem("user"));
    if (!loggedIn) {
      next({ name: "Login" });
    } else {
      next(); // go to wherever I'm going
    }
  } else {
    next(); // does not require auth, make sure to always call next()!
  }
});

export default router;
