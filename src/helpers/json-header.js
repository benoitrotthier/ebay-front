export function jsonHeader() {
  // return content-type header with application/json
  return { "Content-Type": "application/json" };
}
