export const productService = {
  sell,
  auction,
  buy,
  bid,
  getProducts,
  getAllProducts,
  getById,
};

import { authHeader } from "@/helpers";
import { userService } from "@/services";

const apiUrl = "http://localhost:8080";

function sell(
  name,
  description,
  unitPrice,
  currency,
  category,
  seller /*, images*/
) {
  const headers = Object.assign({}, authHeader(), {
    "Content-Type": "application/json",
  });
  const requestOptions = {
    method: "POST",
    headers: headers,
    body: JSON.stringify({
      name,
      description,
      unitPrice,
      currency,
      category,
      seller,
      //images,
    }),
  };

  return fetch(`${apiUrl}/products/sell`, requestOptions).then(handleResponse);
}

function buy(product, user) {
  const productId = product.id;
  const userId = user.id;
  const requestOptions = {
    method: "POST",
    headers: authHeader(),
  };

  return fetch(
    `${apiUrl}/products/buy?productId=${productId}&userId=${userId}`,
    requestOptions
  ).then(handleResponse);
}

function bid(product, user, newPrice) {
  const productId = product.id;
  const userId = user.id;
  const requestOptions = {
    method: "POST",
    headers: authHeader(),
  };

  return fetch(
    `${apiUrl}/products/bid?productId=${productId}&userId=${userId}&newPrice=${newPrice}`,
    requestOptions
  ).then(handleResponse);
}

function auction(name, description, startingPrice, currency, category, seller) {
  const headers = Object.assign({}, authHeader(), {
    "Content-Type": "application/json",
  });
  const requestOptions = {
    method: "POST",
    headers: headers,
    body: JSON.stringify({
      name,
      description,
      startingPrice,
      currency,
      category,
      seller,
      //images,
    }),
  };

  return fetch(`${apiUrl}/products/auction`, requestOptions).then(
    handleResponse
  );
}

function getById(id) {
  const requestOptions = {
    method: "GET",
    headers: authHeader(),
  };

  return fetch(`${apiUrl}/products/${id}`, requestOptions).then(handleResponse);
}

function getProducts(username) {
  const requestOptions = {
    method: "GET",
    headers: authHeader(),
  };

  return fetch(
    `${apiUrl}/users/${username}/productsOnSale`,
    requestOptions
  ).then(handleResponse);
}

function getAllProducts() {
  const requestOptions = {
    method: "GET",
    headers: authHeader(),
  };

  return fetch(`${apiUrl}/products/`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        // auto logout if 401 response returned from api
        userService.logout();
        location.reload();
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
